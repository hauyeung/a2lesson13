package com.example.a2lesson13;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.Bundle;
import android.app.Activity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity{

	private Button gobutton;
	private Button backbutton;
	private Button forwardbutton;
	private Button refreshbutton;
	private Button zoominbutton;
	private Button zoomoutbutton;
	private EditText url;
	private WebView webview;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_PROGRESS);

		setContentView(R.layout.activity_main);
		gobutton = (Button) findViewById(R.id.go_button);
		backbutton = (Button) findViewById(R.id.back_button);
		forwardbutton = (Button) findViewById(R.id.forward_button);
		refreshbutton = (Button) findViewById(R.id.refresh_button);
		zoominbutton = (Button) findViewById(R.id.zoomin_button);
		zoomoutbutton = (Button) findViewById(R.id.zoomout_button);
		url = (EditText) findViewById(R.id.url);
		webview = (WebView) findViewById(R.id.webview);
		webview.setWebViewClient(new WebViewClient() {

			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				// Do something
				System.out.println("error");

			}			
		});

		webview.setWebChromeClient(new WebChromeClient(){
			@Override
			public void onProgressChanged(WebView view, int progress) { 
				setProgress(progress * 100);
			}
		});

		gobutton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				webview.loadUrl(url.getText().toString());

				try {
					HttpClient httpclient = new DefaultHttpClient();
					HttpGet httpget = new HttpGet(url.getText().toString());
					HttpResponse response = httpclient.execute(httpget);
					HttpEntity entity = response.getEntity();

					if (entity == null) {
						Toast.makeText(MainActivity.this, "Error loading page", Toast.LENGTH_LONG).show();
					}

				} catch (Exception e) {
					Toast.makeText(MainActivity.this, "Error loading page", Toast.LENGTH_LONG).show();
				}

			}


		});

		backbutton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				webview.goBack();
			}

		});

		forwardbutton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				webview.goForward();
			}

		});

		refreshbutton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				webview.reload();
			}

		});

		zoominbutton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				webview.zoomIn();
			}

		});

		zoomoutbutton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				webview.zoomOut();
			}

		});

		url.setOnKeyListener(new OnKeyListener(){

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (keyCode == KeyEvent.KEYCODE_ENTER)
				{
					webview.loadUrl(url.getText().toString());


					try {
						HttpClient httpclient = new DefaultHttpClient();
						HttpGet httpget = new HttpGet(url.getText().toString());
						HttpResponse response = httpclient.execute(httpget);
						HttpEntity entity = response.getEntity();

						if (entity == null) {
							Toast.makeText(MainActivity.this, "Error loading page", Toast.LENGTH_LONG).show();
						}

					} catch (Exception e) {
						Toast.makeText(MainActivity.this, "Error loading page", Toast.LENGTH_LONG).show();
					}
				}
				return false;
			}

		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}


}
